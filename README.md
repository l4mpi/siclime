###siclime - Sicstus Prolog Plugin for the SublimeText 2 Editor

####INSTALLATION

Checkout the project, then copy or link the `plugin` folder into your SublimeText Packages directory.

####CONFIGURATION

The settings file can be found under `plugin/siclime.sublime-settings`. Make sure the `prolog_cmd` setting points to your sicstus executable - the default is `/usr/local/sicstus4.2.3/bin/sicstus`.
Besides the sicstus path, you can configure the layouts used for Trace Perspective and Predicate Search; refer to the SublimeText documentation for more information on layouts.

Project settings are used to set the working path of the sicstus interpreter, and the path for the Predicate Cache. Here is an example Project file:

    {
    "settings":
        {
            "sicstus_working_dir": "/home/$user/prog/prolog",
            "analyzer_shelf_path": "/home/$user/prog/prolog/.analyze.shelf"
        }
    }

####USAGE

All global keybindings defined by this plugin are prefixed by Ctrl-P; e.g. spawning the REPL can be achieved by pressing `Ctrl-p + Ctrl-p`. Keybindings specific to the REPL are mostly single buttons.

#####Global Commands
* `Ctrl-p + Ctrl-p`: Spawn the REPL
* `Ctrl-p + Ctrl-b`: Set a Breakpoint at current cursor position (requires running REPL)
* `Ctrl-p + Ctrl-x`: Remove a Breakpoint at current cursor position (requires running REPL)
* `Ctrl-p + Ctrl-a`: Run the Analyzer on the current File
* `Ctrl-p + Ctrl-d`: Run the Analyzer recursively for all Prolog files in a directory
* `Ctrl-p + Ctrl-f`: Search for Predicates (search format: `module:pred_name/arity`)
* `Ctrl-p + Ctrl-n`: Jump to next definition of currently selected Predicate (when a predicate has been chosen from the Predicate View)
* `Ctrl-p + Ctrl-c`: Find all calls for the predicate definition under the cursor
* `Ctrl-p + Ctrl-i`: Predicate autocompletion: Suggest possible completions for the string under cursor
    
#####REPL Commands
* `Ctrl-r`: Recompile source file
* `Ctrl-e`: Jump to source of last error
* `Ctrl-t`: Switch to Trace Perspective
* `Ctrl-s`: Enable Trace Follow Mode (jumps to source position on suspend)
* `Ctrl-f`: Force the REPL into 'read' mode - in some cases SICStus doesn't send a PROMPT or YES/NO event after a procedure has finished, which the REPL needs in order to know it should allow the user to input further queries. If this happens you're stuck with a read-only REPL view - use this command to make it writeable again.
    
`n` and `;` are used to reject a solution, `Enter` accepts it.
During tracing, you can use a subset of the normal sicstus commands; namely `a, c, l, z, s, r, f, g, v, w, <`.
Up and down arrows can be used to access the command history.

#####Predicate View and Finder
Predicates are displayed in the format `[n] module:name/arity`, where `n` is the number of definitions of the predicate. When searching predicates with the Finder, you can enter a search query in the same Format (without the number of definitions!); currently only prefix search for module and name is supported. Pressing enter in the Finder dialog transfers focus to the Predicate View, You can then jump to the first Predicate by simply pressing `Enter`. Press `Ctrl-p + Ctrl-n` to jump to successive definitions.
