from xml import sax

class PrologAnalyzerOutputParser(sax.handler.ContentHandler):

    def parse(self, fname):
        self._charbuffer = []
        self._result = []
        with open(fname) as f:
            sax.parse(f, self)
        return self._result[0]

    def startElement(self, name, attrs):
        getattr(self, "_start_%s" % name, lambda *_: 0)(attrs)

    def endElement(self, name):
        getattr(self, "_end_%s" % name, lambda *_: 0)()

    def characters(self, data):
        self._charbuffer.append(data)

    def _flushCharacters(self):
        res = ''.join(self._charbuffer)
        self._charbuffer = []
        return res

    def _start_programm(self, attrs):
        self._result.append({"file": attrs["file"], "module": attrs["module"], "predicates": []})

    def _start_predicate(self, attrs):
        pred = {"name": attrs["name"], "arity": int(attrs["arity"]), "calls": []}
        pred["startlines"] = [int(x.strip()) for x in attrs["startlines"][1:-1].split(",")]
        pred["endlines"] = [int(x.strip()) for x in attrs["endlines"][1:-1].split(",")]
        self._result[-1]["predicates"].append(pred)

    def _start_call(self, attrs):
        self._result[-1]["predicates"][-1]["calls"].append(dict(attrs))

