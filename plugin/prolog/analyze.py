from threading import Thread
from time import sleep
from subprocess import call
from os import path, remove
from collections import namedtuple, defaultdict

#XXX workaround for shelf corruption under OSX - force shelf to use 'dumbdbm' module
from platform import system as _os_descr
if "darwin" in _os_descr().lower():
    import anydbm
    anydbm._defaultmod = __import__("dumbdbm")
#XXX end workaround

import shelve
from sublime import status_message, set_timeout
from prolog.analyze_parser import PrologAnalyzerOutputParser as PXmlParser

_PredicateDef = namedtuple("PredicateDef", "name module arity file start end")

class _Predicate(namedtuple("Predicate", "name module arity file startlines endlines")):
    def __str__(self):
        return "[%s] %s:%s/%s" % (len(self.startlines), self.module, self.name, self.arity)

    def _lineno_in_predicate(self, lineno):
        return any(s <= lineno <= e for s, e in zip(self.startlines, self.endlines))

_fpath = path.realpath(__file__)

def makeAnalyzer(sicstus_path, shelve_path):

    pl_path = _fpath.rsplit("/", 1)[0] + "/analyzer.pl"

    class Analyzer(Thread):

        def run(self):
            self._initshelf()
            self._parser = PXmlParser()
            self._analyze_requests = []
            self._done = 0
            while not self._done:
                changed = 0
                while self._analyze_requests:
                    self._analyze(self._analyze_requests.pop())
                    changed = 1
                if changed:
                    self._data.sync()
                sleep(0.05)

        def kill(self):
            self._done = 1

        def _initshelf(self):
            self._data = shelve.open(shelve_path, protocol=2, writeback=True)
            if not self._data.get("initialized"):
                self._data["predicates"] = []
                self._data["calls"] = defaultdict(list)
                self._data["pred_by_file"] = defaultdict(list)
                self._data["initialized"] = True

        def start_analyze(self, fpath):
            self._analyze_requests.append(fpath)

        def find_predicates(self, name, module="", arity=None):
            #XXX maybe slow for a huge amount of predicates
            n_len = len(name)
            m_len = len(module)
            #performance optimization to avoid filtering multiple times
            filters = {(0, 0): (p for p in self._data["predicates"] if p.name[:n_len] == name),
                       (1, 0): (p for p in self._data["predicates"] if p.name[:n_len] == name and p.module[:m_len] == module),
                       (0, 1): (p for p in self._data["predicates"] if p.name[:n_len] == name and p.arity == arity),
                       (1, 1): (p for p in self._data["predicates"] if p.name[:n_len] == name and p.module[:m_len] == module and p.arity == arity)}
            #XXX turn generator into list here?
            return filters[(module != "", arity is not None)]

        def get_all_defs(self):
            return self._data["predicates"]

        def get_calls(self, module, pred, arity):
            calls = self._data["calls"][(module, pred, arity)]
            preds = []
            for file in set(c[0] for c in calls):
                fpreds = self._data["pred_by_file"][file]
                preds.extend([p for p in fpreds if (file, p.module, p.name, p.arity) in calls])
            return preds

        def get_pred_by_pos(self, file, line):
            file_pred = self._data["pred_by_file"][file]
            for p in file_pred:
                if p._lineno_in_predicate(line):
                    return p

        def get_calls_by_pos(self, file, line):
            pred = self.get_pred_by_pos(file, line)
            return self.get_calls(pred.module, pred.name, pred.arity) if pred else []

        def _mk_analyze_cmd(self, fpath, resultpath):
            return [sicstus_path, "-l", pl_path, "--goal", "analyze_file('%s','%s'),halt." % (fpath, resultpath)]

        def _analyze(self, fpath):
            resultpath = "%s.xml" % fpath
            with open("/dev/null", "wb") as devnull:
                call(self._mk_analyze_cmd(fpath, resultpath), stdout=devnull, stderr=devnull)
            res = self._parser.parse(resultpath)
            self._update_files(res)
            remove(resultpath)
            set_timeout(lambda: status_message("Analyzed file %s" % fpath), 1)

        def _update_files(self, res):
            self._retract_file_data(res["file"])
            self._process_file(res)

        def _process_file(self, fdata):
            mod = fdata["module"]
            file = fdata["file"]
            for p in fdata["predicates"]:
                pname = p["name"]
                arity = p["arity"]
                pred = _Predicate(pname, mod, arity, file,
                    sorted([x-1 for x in p["startlines"]]), sorted([x-1 for x in p["endlines"]]))
                self._data["predicates"].append(pred)
                self._data["pred_by_file"][file].append(pred)
                for call in p["calls"]:
                    #TODO find actual predicate definition(s) that calls this
                    cmod = call["module"]
                    if cmod == "built_in": continue #not interested in calls to builtins
                    cname = call["name"] if not call["name"] == "RECURSIVE_CALL" else pname
                    self._data["calls"][(cmod, cname, int(call["arity"]))].append((file, mod, pname, arity))

        def _retract_file_data(self, file):
            #remove all information extracted from 'file'
            self._data["predicates"] = [p for p in self._data["predicates"] if not p.file == file]
            if file in self._data["pred_by_file"]:
                del self._data["pred_by_file"][file]
            for k in self._data["calls"].keys():
                self._data["calls"][k] = [c for c in self._data["calls"][k] if not c[0] == file]
                if not self._data["calls"][k]:
                    del self._data["calls"][k]

    a = Analyzer()
    a.start()
    return a

