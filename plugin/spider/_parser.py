class _BaseParser():
    """ a parser that handles different input streams and can line-buffer them.
        This class should only be used as a base class, subclasses need to implement
        the _parse_stream(self, string) method that splits raw data into the stream
        and message. """

    def __init__(self, buffered_streams=[], line_sep="\x00"):
        if self.__class__ == _BaseParser:
            raise TypeError("class _BaseParser is abstract and cannot be instantiated!")
        self._callbacks = set()
        self._streambuffers = dict((stream, []) for stream in buffered_streams)
        self._line_sep = line_sep
        self._init()

    def _init(self):
        """ to be overwritten by subclasses """
        pass

    def register_callback(self, callback):
        self._callbacks.add(callback)

    def unregister_callback(self, callback):
        self._callbacks.remove(callback)

    def parse(self, string):
        stream, message = self._parse_stream(string)
        message = message.replace(self._line_sep, "\n")
        if stream in self._streambuffers:
            self._handle_buffered(stream, message)
        else:
            self._handle(stream, message)

    def _handle_buffered(self, stream, message):
        if not "\n" in message:
            self._streambuffers[stream].append(message)
        else:
            lines = message.split("\n")
            buff = ''.join(self._streambuffers[stream])
            self._streambuffers[stream] = [lines.pop()]  #replace old buffer with last unfinished line
            lines[0] = buff + lines[0]  #add buffer contents to the first line
            [self._handle(stream, line) for line in lines]

    def _handle(self, stream, message):
        handler = getattr(self, "_stream_" + stream, lambda *_: None)
        self._broadcast_event(handler(message))

    def _broadcast_event(self, event):
        if not event: return
        for cb in self._callbacks:
            cb(event)

