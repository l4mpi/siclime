import SocketServer
from socket import AF_INET6
from subprocess import Popen, PIPE
from threading import Thread
from spider.parser import _SpiderParser
from spider.api import _SpiderAPI

_SPIDER_PORT = 9999
_spider_args = ["--spider", "(('':%s),version(1,5))." % _SPIDER_PORT]

class _IPv6TCPServer(SocketServer.TCPServer):
    """ IPv6 TCP Server that ignores TIME_WAIT """
    allow_reuse_addres = True
    address_family = AF_INET6

class SpiderIF(_SpiderAPI):

    def __init__(self, cmd, flags=[], cwd=None):
        self._cmdlist = [cmd] + flags + _spider_args
        self._cwd = cwd
        self._parser = _SpiderParser(["debugEvents", "toplevelEvents"])
        self._register_cmd = self._parser._register_cmd
        self._cmd_n, self._bp_n = 1, 1
        self._breakpoints = {}

    def _run(self):
        self._server = _IPv6TCPServer(("", _SPIDER_PORT), self._handleConnection)
        self._proc = Popen(self._cmdlist, cwd=self._cwd, stdin=PIPE)
        self._server.serve_forever()

    def start(self):
        Thread(target=lambda: self._run()).start()

    def _handleConnection(self, request, _client_addr, _server):
        self._request = request
        while 1:
            data = request.recv(10**5).strip()
            if not data: break
            for line in data.split("\n"): self._parser.parse(line)

    def register_callback(self, cb):
        self._parser.register_callback(cb)

    def close(self):
        self._send_exit()
        self._proc.terminate()
        self._server.shutdown()
        self._server.server_close()
        self._proc.wait()

