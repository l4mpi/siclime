from spider._parser import _BaseParser

def _kvlist_todict(string):
    """ turn a '%'-separated key/value list into a dictionary """
    s = string.split("%")
    return dict(zip(*[s[i::2] for i in [0, 1]]))

def _dict_slice(ddict, keys, optkeys=[]):
    """ returns a subdict of ddict containing the given keys. keys in `optkeys` default to None if not present in the dict """
    return dict([(k, ddict[k]) for k in keys] + [(k, ddict.get(k)) for k in optkeys])

class _Event():
    def __init__(self, e_type, data):
        #print "%s: %s" % (e_type, data)
        self.type = e_type
        self.data = data

class _SpiderParser(_BaseParser):

    def _init(self):
        self._cmds = {}

    def _register_cmd(self, num, ctype):
        self._cmds[num] = ctype

    def _parse_stream(self, string):
        #if not string.startswith("TUNNEL APPEND"):
        #    raise Exception("Cannot handle message: %s" % string)
        #this statement strips the "TUNNEL APPEND " and then partitions the rest into stream and message (where msg can be '')
        return string.split(" ", 2)[2].partition(" ")[::2]

    def _stream_user_error(self, msg):
        """ STDERR stream, used for text output by the sicstus interpreter """
        return _Event("STDERR", msg)

    def _stream_user_output(self, msg):
        return _Event("STDOUT", msg)

    def _stream_debugEvents(self, msg):
        """ some general status and information about the interpreter (version, path, etc.), reply to commands """
        if not " " in msg:
            msg += " "
        mtype, mbody = msg.split(" ", 1)
        return getattr(self, "_debug_" + mtype.lower(), lambda *_: None)(mbody)

    def _debug_reply(self, msg):
        cmd_n, status = msg.split(" ", 1)
        cmdtype = self._cmds.get(int(cmd_n))
        return getattr(self, "_reply_%s" % cmdtype, lambda *_: None)(cmd_n, status)

    def _reply_load(self, cmd_n, status):
        return _Event("REPLY", {"cmd": cmd_n, "status": status})

    def _reply_add_breakpoint(self, cmd_n, message):
        bp_id = message.split(" ")[2]
        return _Event("ADD_BP", {"id": bp_id})

    def _reply_remove_breakpoint(self, cmd_n, message):
        bp_id = message.split(" ")[3]
        return _Event("REMOVE_BP", {"id": bp_id})

    def _reply_stack(self, cmd_n, message):
        """ stack is expressed as a '#'-separated list of stack entries, which are '%'-separated lists containing file name, predicate, etc """
        stackentries = message.split("#")
        stack = []
        for entry in reversed(stackentries):   #iterate reversed to start at the top of the stack
            e = entry.split("%")
            stack.append({"file": e[0], "line": e[1], "predicate": e[3], "arity": e[4]}) #TODO what else do we need / have?
        return _Event("STACK", stack)

    def _debug_debuggerevent(self, msg):
        if msg.startswith("suspend"):
            return _Event("TRACE_SUSPEND", None)
        if msg.startswith("resume"):
            return _Event("TRACE_RESUME", msg)

    def _stream_toplevelEvents(self, msg):
        """ events from the prolog interpreter (repl status, errors, solutions, etc) """
        if msg.startswith("TOPLEVELEVENT ITEMS"):
            items = _kvlist_todict(msg.split(" ", 2)[2])
        else:
            raise Exception("Unknown TLE: '%s'" % msg)
        return getattr(self, "_functor_" + items["functor"], lambda *_: None)(items)

    #TODO find out which of these functors we actually need

    def _functor_version(self, items):
        #version information of the interpreter
        pass

    def _functor_no(self, items):
        #no more solutions
        pass

    def _functor_yes(self, items):
        #user accepted solution
        pass

    def _functor_existence_error(self, items):
        """ referenced nonexistant predicate """
        if not "message" in items:
            items["message"] = "Existence Error"
        return _Event("EX_ERROR", _dict_slice(items, ["message"], ["goal", "culprit"]))

    def _functor_syntax_error(self, items):
        """ invalid syntax """
        file = items.get("file") #can be None for Syntax Errors in the REPL
        #if line numbers are present, convert to int; otherwise put None
        lfrom, lto = items.get("linefrom"), items.get("lineto")
        lfrom, lto = lfrom and int(lfrom), lto and int(lto)
        data = {"file": file, "from": lfrom, "to": lto, "msg": items["message"]}
        return _Event("SYN_ERROR", data)

    def _functor_source_pos(self, items):
        """ source position information (e.g. existence error) """
        fileref = items["srcinfo"]
        if not fileref.startswith("fileref("):
            #cannot parse the srcinfo item
            #TODO print a warning message?
            return
        file, line = fileref[8:-1].rsplit(",", 1)
        if file[0] == "'": file = file[1:-1]
        return _Event("SOURCE_POS", {"file": file, "line": int(line)})

    def _functor_no_source_pos(self, items):
        #no source position (e.g. existence error in repl input)
        pass

    def _functor_loading(self, items):
        return _Event("LOADING", _dict_slice(items, ["file", "depth"]))

    def _functor_loaded(self, items):
        """ finished compiling a file """
        return _Event("LOADED", _dict_slice(items, ["file"]))

    def _functor_prompt(self, items):
        """ prompting the user """
        return _Event("PROMPT", {})

    def _functor_ide_message(self, items):
        #everything else -.-
        if items["term"].startswith("ide_message(solutions("):
            return _Event("SOLUTION", {})

