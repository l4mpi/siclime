def _registerCmd(cmd_t):
    def _deco(fct):
        def f(self, *a):
            self._register_cmd(self._cmd_n, cmd_t)
            fct(self, *a)
        return f
    return _deco

class _SpiderAPI():

    def _register_cmd(self, num, ctype):
        pass

    def sendToStdin(self, data):
        self._proc.stdin.write(data)

    def sendLine(self, data):
        self.sendToStdin(data + "\n")

    def _send(self, data):
        self._request.sendall(data)

    def _sendcmd(self, content):
        r = "DO Actions=[show(silent),command(ask)], %s" % (content)
        self._sendrequest(r)

    def _sendrequest(self, content):
        r = "REQUEST %s %s\n" % (self._cmd_n, content)
        self._send(r)
        self._cmd_n += 1

    @_registerCmd("load")
    def send_load(self, file):
        self._sendcmd("compile([(:(user,'%s'))])." % file)

    def send_trap_errors(self):
        self._sendcmd("ide_do(trap_errors_on).")

    @_registerCmd("add_breakpoint")
    def _send_breakpoint(self, name, definition):
        self._breakpoints[name] = self._bp_n
        cmd = "ide_do(add_breakpoint([%s,id('%s')], Reply),Reply,_)." % (definition, self._bp_n)
        self._sendcmd(cmd)
        self._bp_n += 1

    def send_breakpoint_line(self, file, line):
        bp_name = "%s:%s" % (file, line)
        cmd = "line(%s),file('%s'),uri('file:%s')" % (line, file, file)
        self._send_breakpoint(bp_name, cmd)
        return bp_name

    def send_breakpoint_predicate(self, name, arity):
        bp_name = "/(%s, %s)" % (name, arity)
        cmd = "test([pred(:(user, /(%s, %s)))])" % (name, arity)
        self._send_breakpoint(bp_name, cmd)
        return bp_name

    @_registerCmd("remove_breakpoint")
    def _send_breakpoint_remove(self, bp_name):
        cmd = "ide_do(remove_breakpoint([id('%s')], Reply),Reply,_)." % self._breakpoints[bp_name]
        self._sendcmd(cmd)

    def send_breakpoint_rmline(self, file, line):
        bp_name = "%s:%s" % (file, line)
        self._send_breakpoint_remove(bp_name)
        return bp_name

    def send_breakpoint_rmpred(self, name, arity):
        bp_name = "/(%s, %s)" % (name, arity)
        self._send_breakpoint_remove(bp_name)
        return bp_name

    @_registerCmd("stack")
    def send_stack(self):
        self._sendrequest("stack")

    def _send_exit(self):
        self._sendrequest("exit 0 .")

