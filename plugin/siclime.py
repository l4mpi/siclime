import os
import sublime, sublime_plugin
from repl.view import makeReplView
from prolog.analyze import makeAnalyzer
import util

CONFIG_FILE = "siclime.sublime-settings"

_replView = None
_analyzer = None
_predicateView = None

class SiclimeSpawnAnalyzerCommand(sublime_plugin.WindowCommand):
    def run(self):
        global _analyzer
        views = self.window.views()
        if views:
            shelf_path = views[0].settings().get("analyzer_shelf_path")
            if not shelf_path:
                print "ERROR: no shelf path for sicstus analyzer found in project settings!"
            else:
                settings = sublime.load_settings(CONFIG_FILE)
                _analyzer = makeAnalyzer(settings.get("prolog_cmd"), shelf_path)

def _initAnalyzer():
    window = sublime.active_window()
    if window:
        window.run_command("siclime_spawn_analyzer")
    else:
        sublime.set_timeout(lambda: _initAnalyzer(), 100)

sublime.set_timeout(lambda: _initAnalyzer(), 20)

def _replRunning(fct):
    """ decorator ensuring the REPL is running """
    def f(*a):
        if _replView is None:
            sublime.error_message("Prolog not running! You need to spawn a REPL before using this command.")
        else:
            fct(*a)
    return f

def _analyzerRunning(fct):
    """ decorator ensuring the Analyzer is initialized """
    def f(*a):
        if _analyzer is None:
            sublime.error_message("Analyzer not initialized!")
        else:
            fct(*a)
    return f

def _eventViewHasSetting(setting):
    """ decorator for EventListener member functions ensuring the Events view has the required setting """
    def deco(fct):
        def f(listener, view, *a):
            if view.settings().get(setting):
                return fct(listener, view, *a)
        return f
    return deco

def _spawnrepl(window, fpath=None):
    global _replView
    settings = sublime.load_settings(CONFIG_FILE)
    prolog_cmd = settings.get("prolog_cmd")
    prolog_cwd = window.active_view().settings().get("sicstus_working_dir")
    flags = settings.get("interpreter_flags")
    view = util.new_view(window, "PROLOG REPL")
    view.settings().set("siclime_repl_view", True)
    _replView = makeReplView(prolog_cmd, prolog_cwd, flags, fpath, view)

class SiclimeReplViewListener(sublime_plugin.EventListener):
    def on_modified(self, view):
        view.erase_regions("siclime")

    @_eventViewHasSetting("siclime_repl_view")
    def on_selection_modified(self, view):
        _replView.modified()

    def on_close(self, view):
        if view.settings().get("siclime_repl_view"):
            global _replView
            _replView.kill()
            _replView = None
        elif view.settings().get("siclime_predicate_view"):
            global _predicateView
            _predicateView = None

    @_eventViewHasSetting("siclime_repl_view")
    def on_query_context(self, view, key, *a):
        return _replView._get_context(key)

class SiclimePrologAnalyzerListener(sublime_plugin.EventListener):
    def on_post_save(self, view):
        if sublime.load_settings(CONFIG_FILE).get("analyze_on_save"):
            file = view.file_name()
            if "." in file and file.rsplit(".", 1)[1] == "pl": #TODO check the file is in project dir?
                _analyzer.start_analyze(file)

class SiclimeFileReplCommand(sublime_plugin.WindowCommand):
    def run(self):
        if _replView:
            sublime.error_message("REPL already running! Kill the old REPL before spawning a new one.")
        else:
            fpath = self.window.active_view().file_name()
            _spawnrepl(self.window, fpath)

class SiclimeReplEnterCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        cursor_pos = util.view_cursor_pos(self.view)
        _replView.onEnter(cursor_pos)

class SiclimeReplNoCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.send_fail()

class SiclimeReplLasterrCommand(sublime_plugin.WindowCommand):
    def run(self):
        _replView.jumpToError(self.window)

class SiclimeReplBackspaceCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.doBackspace()

class SiclimeReplHistoryUpCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.doHistoryUp()

class SiclimeReplHistoryDownCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.doHistoryDown()

class SiclimeReplKillCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.kill()

class SiclimeReplForcePromptCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.force_prompt()

class SiclimeReplRecompileCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        _replView.send_reload()

class SiclimeReplTracePerspectiveCommand(sublime_plugin.WindowCommand):
    @_replRunning
    def run(self):
        settings = sublime.load_settings(CONFIG_FILE)
        layout = settings.get("trace_layout")
        view_grp = settings.get("trace_grp_repl")
        repl_view = self.window.active_view()
        self.window.set_layout(layout)
        self.window.set_view_index(repl_view, view_grp, 0)
        for view in self.window.views():
            if view.settings().get("siclime_repl_view"): continue
            if self.window.get_view_index(view)[0] == view_grp:
                self.window.set_view_index(view, view_grp - 1, len(self.window.views_in_group(view_grp - 1)))
        _replView.trace_source_pos(True)

class SiclimeBreakpointLineCommand(sublime_plugin.TextCommand):
    @_replRunning
    def run(self, edit):
        line = util.view_lineno(self.view) + 1 #sublime lines start at 0, sicstus lines at 1
        bp_name = _replView.add_breakpoint_line(self.view.file_name(), line)
        util.view_draw_breakpoint(self.view, bp_name)

class SiclimeRemoveBreakpointLineCommand(sublime_plugin.TextCommand):
    @_replRunning
    def run(self, edit):
        line = util.view_lineno(self.view) + 1
        bp_name = _replView.remove_breakpoint_line(self.view.file_name(), line)
        util.view_erase_breakpoint(self.view, bp_name)

class SiclimeReplTraceSourceCommand(sublime_plugin.TextCommand):
    @_replRunning
    def run(self, _):
        _replView.trace_source_pos()

def _spawnPredicateView(window):
    active_view = window.active_view()
    settings = sublime.load_settings(CONFIG_FILE)
    layout = settings.get("search_layout")
    view_grp = settings.get("search_grp")
    _pview = util.new_view(window, "predicates")
    _pview.settings().set("siclime_predicate_view", True)
    window.set_layout(layout)
    window.set_view_index(_pview, view_grp, 0)
    for view in window.views():
        if view.settings().get("siclime_predicate_view"): continue
        if window.get_view_index(view)[0] == view_grp:
            window.set_view_index(view, view_grp - 1, len(window.views_in_group(view_grp - 1)))
    if active_view: window.focus_view(active_view)
    window.focus_view(_pview)
    return _pview

class SiclimeAnalyzePrologFileCommand(sublime_plugin.TextCommand):
    @_analyzerRunning
    def run(self, _):
        _analyzer.start_analyze(self.view.file_name())

class SiclimeAnalyzePrologDirCommand(sublime_plugin.WindowCommand):
    @_analyzerRunning
    def run(self):
        def analyze_dir(dirpath):
            for dpath, _, fs in os.walk(dirpath):
                for file in [x for x in fs if x.endswith(".pl")]:
                    _analyzer.start_analyze(os.path.join(dpath, file))
        self.window.show_input_panel("Enter Directory Path to analyze", "", analyze_dir, None, None)

class SiclimeFindPredicateCommand(sublime_plugin.WindowCommand):
    @_analyzerRunning
    def run(self):
        global _predicateView
        _pview = _spawnPredicateView(self.window)
        pfinder = util.PredicateFinderHelper(_analyzer.get_all_defs(), _pview)
        _predicateView = pfinder._view
        self.window.show_input_panel("predicate search:", "", pfinder.on_done, pfinder.on_change, pfinder.on_cancel)

class SiclimePredicateGoCommand(sublime_plugin.WindowCommand):
    def run(self):
        _predicateView.goto_predicate(self.window)

class SiclimePredicateNextDefCommand(sublime_plugin.WindowCommand):
    def run(self):
        if _predicateView and _predicateView._lineno > -1:
            _predicateView.goto_next_def(self.window)

class SiclimePredicateCallsCommand(sublime_plugin.WindowCommand):
    @_analyzerRunning
    def run(self):
        global _predicateView
        view = self.window.active_view()
        file = view.file_name()
        line = util.view_lineno(view)
        preds = _analyzer.get_calls_by_pos(file, line)
        if preds:
            _predicateView = util.PredicateView(_spawnPredicateView(self.window), preds)
            _predicateView.activate()

class SiclimeAutocompleteCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.run_command("expand_selection", {"to": "word"})
        word_region = self.view.sel()[0]
        preds = list(_analyzer.find_predicates(self.view.substr(word_region)))
        if not preds: return
        def _replace(index):
            if not index == -1:
                p = preds[index]
                self.view.replace(edit, word_region, "%s(%s)" % (p.name, ','.join("X%s" % n for n in range(p.arity))))
        self.view.window().show_quick_panel(["%s/%s" % (p.name, p.arity) for p in preds], _replace)

#GENERAL TRACE COMMANDS - these all just send a key to the repl, so there's no use in declaring them by hand
trace_cmds = {"creep": "c", "leap": "l", "zip": "z", "skip": "s", "retry": "r", "abort": "a",
              "fail": "f", "ancestors": "g", "variables": "v", "write": "w"}

def _mk_trace_run(key):
    return lambda _1, _2: _replView.send_dbg(key)

for cmd, key in trace_cmds.items():
    """ create a new Command class that sends the commands' key to the repl """
    cmdname = "SiclimeReplDbg%sCommand" % cmd.capitalize()
    globals()[cmdname] = type(cmdname, (sublime_plugin.TextCommand, ), {"run": _mk_trace_run(key)})

#SPECIAL TRACE COMMANDS - these need some sort of special handling or produce side effects

class SiclimeReplDbgVariablesCommand(sublime_plugin.TextCommand):
    def run(self, _):
        _replView.doTraceVariables()

class SiclimeReplDbgPrintDepthCommand(sublime_plugin.TextCommand):
    def run(self, _):
        def f_done(text):
            _replView.send_dbg("<" + text)
        self.view.window().show_input_panel("Enter Print Depth", "", f_done, None, None)

