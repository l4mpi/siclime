class _HandlerMixin():
    def callback(self, event):
        getattr(self, "_handle_" + event.type, lambda _: None)(event.data)

class SpiderViewHandler(_HandlerMixin):
    """ parse repl output for errors/warnings or events like returning control to the user """
    def __init__(self, replview):
        self._rv = replview
        self._src_pos_handler = lambda *_: None

    def _mk_error(self, etype, message):
        self._rv.error_info = {"info": "%s Error: %s" % (etype, message)}

    def _set_err_pos(self, file, lfrom, lto=None):
        self._rv.error_info.update({"file": file, "from": lfrom, "to": lto})

    def _handle_STDERR(self, msg):
        self._rv._write(msg)

    _handle_STDOUT = _handle_STDERR

    def _handle_PROMPT(self, _):
        if self._rv.rmode == "init":
            self._rv._spider.send_trap_errors()
            if self._rv._file: self._rv.send_reload()
        self._rv.rmode = "r"

    def _handle_SOLUTION(self, _):
        if not self._rv.rmode == "t_variables":
            self._rv.rmode = "p"

    def _handle_SYN_ERROR(self, data):
        self._mk_error("Syntax", data["msg"])
        if data["file"]:
            self._set_err_pos(data["file"], data["from"], data["to"])
        else:
            self._rv.rmode = "r"

    def _handle_EX_ERROR(self, data):
        self._src_pos_handler = self._src_pos_EX_ERROR
        self._mk_error("Existence", data["message"])
        self._rv.rmode = "r"

    def _src_pos_EX_ERROR(self, data):
        self._set_err_pos(data["file"], data["line"])

    def _handle_SOURCE_POS(self, data):
        self._rv._handle_source_pos(data)
        self._src_pos_handler(data)
        self._src_pos_handler = lambda *_: None

    def _handle_REPLY(self, data):
        if not data["status"] == "OK":
            print("WARNING: command %s failed with status %s!" % (data["cmd"], data["status"]))

    def _handle_TRACE_SUSPEND(self, _):
        self._rv.rmode = "t"

    def _handle_LOADING(self, data):
        if data["depth"] == "0":
            self._rv.error_info = {}

