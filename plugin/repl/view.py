from time import sleep
from codecs import getincrementaldecoder
from sublime import set_timeout, status_message
import util
from spider.interface import SpiderIF
from handler import SpiderViewHandler

def makeReplView(prolog_cmd, cwd, flags, fpath, view):
    spider = SpiderIF(prolog_cmd, flags, cwd)
    rv = _ReplView(view, spider, fpath)
    handler = SpiderViewHandler(rv)
    spider.register_callback(handler.callback)
    spider.start()
    return rv

class _ReplView():
    def __init__(self, view, spider, file):
        self._view = view
        self._spider = spider
        self._file = file
        self.rmode = "init"
        self.error_info = {}
        self._dec = getincrementaldecoder("utf-8")()
        self._cc = 0 #character count
        self._stderr = []
        self._history = util.HistoryHelper()
        self._done = 0
        self._trace_follow = 0
        set_timeout(self._update_callback(), 100)

    def _write(self, string):
        if not string: return
        self._stderr.append(self._dec.decode(string))

    def _update_callback(self):
        def _update_callback():
            if self._stderr:
                strings, self._stderr = self._stderr, []
                self._cc = util.view_write_batch(self._view, strings, self._cc)
                util.view_set_cursor(self._view, self._cc)
            if not self._done: set_timeout(_update_callback, 50)
        return _update_callback

    def onEnter(self, cpos):
        string = util.view_substr(self._view, self._cc, cpos)
        self._cc += len(string)
        if self.rmode == "r":
            self._history.add(string) #add the string to the history buffer
            self._stderr.append("\n") #insert newline after user input
            if string.strip().endswith("."):
                self.rmode = "e"
            self._spider.sendLine(string)
        else:
            self._spider.sendLine("")
            if self.rmode == "t_variables":
                self.rmode = "t"

    def send_fail(self):
        self._stderr.append(";")
        self._spider.sendLine(";")

    def send_dbg(self, cmd):
        self._stderr.append(cmd)
        self._spider.sendLine(cmd)

    def doBackspace(self):
        if self._view.sel()[0].begin() > self._cc:
            self._view.run_command("left_delete")

    def _doHistory(self, string):
        util.view_delete(self._view, self._cc)
        if string:
            util.view_write(self._view, string, self._cc)

    def doHistoryUp(self):
        string = self._history.hist_up()
        self._doHistory(string)

    def doHistoryDown(self):
        string = self._history.hist_down()
        self._doHistory(string)

    def doTraceVariables(self):
        self.rmode = "t_variables"
        self.send_dbg("v")

    def send_reload(self):
        self._spider.send_load(self._file)

    def force_prompt(self):
        self._stderr.append("\n")
        self._spider.sendLine("")
        self.rmode = "r"

    def add_breakpoint_line(self, file, line):
        return self._spider.send_breakpoint_line(file, line)

    def remove_breakpoint_line(self, file, line):
        return self._spider.send_breakpoint_rmline(file, line)

    def modified(self):
        self._view.set_read_only((not self.rmode == "r") or self._view.sel()[0].begin() < self._cc)

    def _get_context(self, key):
        if key.startswith("replmode"):
            return self.rmode == key.split("_", 1)[-1]

    def kill(self):
        if self._done: return
        self._spider.close()
        sleep(0.1)
        self._stderr.append("\nREPL Killed")
        self._done = 1
        set_timeout(lambda: self._view.set_read_only(True), 10)

    def jumpToError(self, window):
        file = self.error_info.get("file")
        if not file: return
        util.file_highlight_lines(window, file, self.error_info["from"], self.error_info["to"])
        set_timeout(lambda: status_message(self.error_info["info"]), 1)

    def _source_pos_callback(self, data):
        window = self._view.window()
        view = util.get_or_create_view(window, data["file"])
        region = util.view_lineno_to_region(view, data["line"] - 1)
        fct = lambda: util.view_add_region(view, "siclime", region, "trace")
        if not view.is_loading():
            fct()
        else:
            set_timeout(fct, 50)
        window.focus_view(self._view)

    def trace_source_pos(self, trace_state=None):
        if trace_state is None:
            self._trace_follow = not self._trace_follow
        else:
            self._trace_follow = trace_state

    def _handle_source_pos(self, data):
        if self._trace_follow:
            set_timeout(lambda: self._source_pos_callback(data), 1)

