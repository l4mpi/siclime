from collections import deque
from sublime import Region, HIDDEN, PERSISTENT, set_timeout

def view_cursor_pos(view):
    return view.sel()[0].begin()

def view_lineno(view):
    return view.rowcol(view_cursor_pos(view))[0]

def view_lineno_to_region(view, lineno):
    return view.line(view.text_point(lineno, 0))

def view_lines_region(view, lfrom, lto):
    #TODO the Region below is one character short if `lto` is the last line and is not followed by a newline
    return Region(view.text_point(lfrom - 1, 0), view.text_point(lto, 0) - 1)

def view_substr(view, cfrom, cto):
    return view.substr(Region(cfrom, cto))

def view_select(view, region):
    view.sel().clear()
    view.sel().add(region)

def view_set_cursor(view, pos):
    r = Region(pos)
    view_select(view, r)
    view.show(r)

def view_write(view, string, cur_pos):
    return view_write_batch(view, [string], cur_pos)

def view_write_batch(view, strings, cur_pos):
    view.set_read_only(False)
    edit = view.begin_edit()
    try:
        for string in strings:
            view.insert(edit, cur_pos, string)
            cur_pos += len(string)
    finally:
        view.end_edit(edit)
    return cur_pos

def view_delete(view, pos_from, pos_to=-1):
    if pos_to == -1:
        pos_to = view.text_point(2**30, 1) #TODO breaks on files with more than 2**30 lines :)
    del_region = Region(pos_from, pos_to)
    edit = view.begin_edit()
    try:
        view.erase(edit, del_region)
    finally:
        view.end_edit(edit)

def view_draw_breakpoint(view, name):
    view.add_regions(str(name), [view.sel()[0]], "breakpoint", "dot", HIDDEN | PERSISTENT)

def view_erase_breakpoint(view, name):
    view.erase_regions(name)

def new_view(window, title, scratch=True):
    view = window.new_file()
    view.set_scratch(scratch)
    view.set_name(title)
    return view

def close_view(view):
    #WHY THE FUCK IS THERE NO BUILT IN FUNCTIONALITY FOR THIS?
    vid = view.id()
    window = view.window()
    av = window.active_view()
    aid = av.id()
    if not aid == vid:
        window.focus_view(view)
    window.run_command("close")
    if not aid == vid:
        window.focus_view(av)

def file_highlight_lines(window, filename, linefrom, lineto):
    if not lineto: lineto = linefrom
    view = get_or_create_view(window, filename)
    #we need a second view (any view in the same window, really) for the hack below
    switch_view = window_get_diff_view(window, view)
    def highlight_error_cb():
        #another hack because of sublime: views returned by `open_file` only load their content after the python method returns
        #thus we need to highlight and jump to the error in a callback to ensure the file is actually loaded -.-
        line_region = view_lines_region(view, linefrom, lineto)
        view_select(view, line_region)
        view.show(line_region)
        #switch views to make sure the selection is drawn
        window.focus_view(switch_view)
        window.focus_view(view)
    set_timeout(highlight_error_cb, 10)

def get_or_create_view(window, filename):
    view = window.find_open_file(filename)
    if not view:
        view = window.open_file(filename)
        window.set_view_index(view, 0, len(window.views_in_group(0)))
    window.focus_view(view)
    return view

def window_get_diff_view(window, view):
    #returns a view that is not the same as the given one (for the various focus-switch/redraw hacks)
    #assumes there exists more than one view; returns None otherwise
    for dview in window.views():
        if not view.id() == dview.id():
            return dview

def view_add_region(view, name, region, scope):
    view.add_regions(name, [region], scope)
    view.show(region)

class HistoryHelper(object):
    def __init__(self):
        self._history = deque()
        self._histindex = -1

    def hist_up(self):
        if self._histindex + 1 < len(self._history):
            self._histindex += 1
        return self._history[self._histindex]

    def hist_down(self):
        if self._histindex > 0:
            self._histindex -= 1
            return self._history[self._histindex]
        else:
            self._histindex = -1
            return ""

    def add(self, entry):
        self._histindex = -1
        if entry.strip():
            self._history.appendleft(entry)

class PredicateFinderHelper(object):
    def __init__(self, predicates, view):
        self._predicates = predicates
        self._last_predicates = []
        self._last = ("", "", "")
        self._view = PredicateView(view)

    def _sub_of_last(self, mod, name, arity):
        #check if the current filter values result in a subset of the last values
        return (mod.startswith(self._last[0])
               and name.startswith(self._last[1])
               and (self._last[2] in (None, arity)))

    def _filter(self, module, name, arity):
        fpred = self._last_predicates if self._sub_of_last(module, name, arity) else self._predicates
        n_len = len(name)
        m_len = len(module)
        #performance optimization to avoid filtering multiple times
        filters = {(0, 0): (p for p in fpred if p.name[:n_len] == name),
                   (1, 0): (p for p in fpred if p.name[:n_len] == name and p.module[:m_len] == module),
                   (0, 1): (p for p in fpred if p.name[:n_len] == name and p.arity == arity),
                   (1, 1): (p for p in fpred if p.name[:n_len] == name and p.module[:m_len] == module and p.arity == arity)}
        #XXX turn generator into list here?
        return filters[(module != "", arity is not None)]

    def on_change(self, string):
        name = (":%s/" % string).rsplit(":", 1)[1].split("/", 1)[0]
        mod = string.split(":", 1)[0] if ":" in string else ""
        try:
            arity = int(string.rsplit("/", 1)[1]) if "/" in string[:-1] else None
        except ValueError:
            arity = None
        pred = list(self._filter(mod, name, arity))
        self._last_predicates = pred
        self._last = mod, name, arity
        #if len(pred) < 100: self._view.display(pred)
        self._view.display(pred)

    def on_done(self, string):
        self._view.activate()

    def on_cancel(self):
        self._view.close()

class PredicateView(object):
    def __init__(self, view, preds=[]):
        self._view = view
        view.set_read_only(True)
        self._active = False
        self._lineno = -1
        if preds: self.display(preds)

    def display(self, predicates):
        self._predicates = predicates
        self._view.set_read_only(False)
        view_delete(self._view, 0) #clear the view
        data = '\n'.join([str(p) for p in predicates])
        view_write_batch(self._view, data, 0)
        self._view.set_read_only(True)

    def activate(self):
        view_set_cursor(self._view, 0)
        self._view.settings().set("predicate_view_active", True)

    def deactivate(self):
        self._view.settings().set("predicate_view_active", 0)

    def close(self):
        if self._view.window():
            #if window is none, User already closed the view before canceling the dialog
            close_view(self._view)

    def goto_predicate(self, window):
        lineno = view_lineno(self._view)
        self._lineno = lineno
        self._current_def = 0
        self._show_pred(window)

    def goto_next_def(self, window):
        self._current_def = (self._current_def + 1) % len(self._predicates[self._lineno].startlines)
        self._show_pred(window)

    def _show_pred(self, window):
        predicate = self._predicates[self._lineno]
        start, end = predicate.startlines[self._current_def], predicate.endlines[self._current_def]
        file_highlight_lines(window, predicate.file, start + 1, end + 1)

